import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HomeService } from '../home/home.service';
import { SharecartService } from '../shared/sharecart.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  productslists:any= [];
  productslist:any = [];
  searchText: string;
  @Output() cartcount = new EventEmitter();
  constructor(private homeservice : HomeService, private shareservice : SharecartService) { }

  addingitems:any =[];
  ngOnInit() {
    if(localStorage.getItem('product')){
      this.addingitems= JSON.parse(localStorage.getItem('product'));
    }
    
    this.homeservice.fngetproductlists().subscribe(data => {
      this.productslist = this.productslists = data;
    });
  }

  addtocart(item){
    if(this.addingitems.indexOf(item) !== -1){
      Swal.fire({
        title: 'Item Exists',
        text: 'Item is already present in the cart',
        icon: 'warning',
        showCancelButton: false,
        allowOutsideClick:false,
      });
    }else{
      this.addingitems.push(item);
      this.shareservice.fngetcartsliststored(this.addingitems.length);
      localStorage.setItem('product',JSON.stringify(this.addingitems));
    }
  }

  fnsearchname(){
    this.productslist = this.productslists.filter(res=>{
      return res.pname.toLocaleLowerCase().match(this.searchText.toLocaleLowerCase());
    });

    //console.log(this.productslist);
  }

}
