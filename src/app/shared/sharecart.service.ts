import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharecartService {
  $emittedcartcount = new EventEmitter();
  constructor() { }
  
  fngetcartslist(){
    return JSON.parse(localStorage.getItem('product'));
  }
  fngetcartsliststored(count){
    this.$emittedcartcount.emit(count)
  }
}
