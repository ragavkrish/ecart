import { Component, OnInit } from "@angular/core";
import { HomeService } from './home.service';

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  

  productslist: any = [];

  constructor(private homeservice : HomeService) {}

  ngOnInit() {
    
    this.homeservice.fngetproductlists().subscribe(data => {
      this.productslist = data;
    });
  }

}
