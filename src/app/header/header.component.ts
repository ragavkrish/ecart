import { Component, OnInit, Input } from '@angular/core';
import { SharecartService } from '../shared/sharecart.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  cartitems:any;
  
  productscount:Number;
  cartitem:any = [];

  constructor(private sharedservice : SharecartService) { }

  ngOnInit() {
    this.cartitems=JSON.parse(localStorage.getItem('product'));
    if(this.cartitems){
      this.productscount= this.cartitems.length;
    }
    this.sharedservice.$emittedcartcount.subscribe(data => {
      this.productscount = data;
    });
  }

}
