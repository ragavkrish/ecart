import { Component, OnInit, Input } from "@angular/core";
import { SharecartService } from '../shared/sharecart.service';

import Swal from 'sweetalert2'
import { Router } from '@angular/router';

@Component({
  selector: "app-cart",
  templateUrl: "./cart.component.html",
  styleUrls: ["./cart.component.css"]
})
export class CartComponent implements OnInit {
  cartitem: any = [];
  cartitems :any = [];
  emptycart:boolean = false;
  total:any;
  items:any = [];
  Quantity:any=[];
  grandtotal:Number=0;
  constructor(private sharedservice : SharecartService, private router : Router) {}

  ngOnInit() {
    this.loadCart();
  }

  loadCart(): void {
		this.total = 0;
    this.items = [];
    if(localStorage.getItem('product')){
      this.cartitem = JSON.parse(localStorage.getItem('product'));
    }
    
    this.sharedservice.fngetcartsliststored(this.cartitem.length);
		for (var i = 0; i < this.cartitem.length; i++) {
			this.cartitems.push({
				"id": this.cartitem[i].id,
        "pname": this.cartitem[i].pname,
        "price":this.cartitem[i].price,
        "ogprice": this.cartitem[i].ogprice,
        "imagename": this.cartitem[i].imagename,
        "totalprice":this.cartitem[i].price,
      });
      this.grandtotal+=this.cartitem[i].price;
   }
	}


  removeitem(index) {
    if (index > -1) {
      this.cartitems.splice(index, 1);
      localStorage.clear();
      localStorage.setItem('product',JSON.stringify(this.cartitems));
      this.sharedservice.fngetcartsliststored(this.cartitems.length);
    }
    this.grandtotal=0;
    for (var i = 0; i < this.cartitems.length; i++) {
      this.grandtotal+=this.cartitems[i].totalprice;
    }
  }

  quantitychange(val,ind){
    // console.log(val,i);
    console.log(this.cartitems[ind].price);
    this.cartitems[ind].totalprice=this.cartitems[ind].price*val;
    this.grandtotal=0;
    for (var i = 0; i < this.cartitems.length; i++) {
      this.grandtotal+=this.cartitems[i].totalprice;
    }
  }

  finalCheckout(){
    console.log(this.cartitems,this.grandtotal,this.cartitems.length);
    Swal.fire({
      title: 'Order Confirmation',
      text: 'Total Amount - ' +this.grandtotal,
      icon: 'warning',
      showCancelButton: true,
      allowOutsideClick:false,
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Order Placed!',
          'Order has been placed',
          'success'
        );
        
      } 
      
    })
  }
}
